# README #

This repository contains different home-made helper command-line utilities and plugins for my everyday developer needs.

Everything is provided as-is. You are free to use it at your own risk.

Have a nice day!

### json-format.php ###

Command-line utility, formats JSON strings from stdin.

Supported output variants: one line / pretty print, escape / unescape non-ascii characters.

Usage: `cat yourfile.json | json-format.php [--one-line] [--escape]`

### pu2png ###

Shortcut script, converts PlantUML diagrams to png file.

PlantUML JAR is required at path /opt/plantuml/plantuml.jar [http://plantuml.com/download]

Usage: `pu2png /path/to/plantuml.pu`

Result: Created new file /path/to/plantuml.pu.png


