#!/usr/bin/php
<?php
/**
 * Command-line utility to format JSON strings from stdin.
 * 
 * @author Evgenii Smolin <esmolin@inbox.ru>
 */
$options = 0;
if (!in_array('--one-line', $argv)) {
    $options |= JSON_PRETTY_PRINT;
}

if (!in_array('--escape', $argv)) {
    $options |= JSON_UNESCAPED_UNICODE;
}

if (in_array('--help', $argv) || in_array('-h', $argv)) {
    fputs(STDERR, "Command-line utility to format JSON strings from stdin.\nUsage: cat file.json | json-format.php [--one-line] [--escape]\n");
    die();
}

$input = file_get_contents("php://stdin");
if ($input) {
    print json_encode(json_decode($input), $options);
};


